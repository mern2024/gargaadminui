import React, { useEffect, useState } from "react";
import Card from "react-bootstrap/Card";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Table from "react-bootstrap/Table";
import { fetchCandiData, postCandiData } from "./CandidateApi";
import axios from "axios";
import { useRef } from "react";

const Candidate = () => {
  const [userdata, setUserData] = useState({
    fullName: "",
    userName: "",
    gender: "",
    mailId: "",
    otp: "",
    password: "",
  });

  const [errors, setErrors] = useState({});
  const[onedit,setOnEdit]=useState(false);
  const [key, setKey] = useState("list-view");
  const[candidate, setCandidate] = useState();

  //! initiazing state for file
  const inputRef = useRef(null);
  const [fileName, setFileName] = useState("");
  const [selectedFile, setselectedFile] = useState(null);

  //!Tokens and Headers
  const user = JSON.parse(sessionStorage.getItem("user"));
  // console.log(user);
  const headers = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

    //!onchange for submit data
  const changehandler = (e) => {
    setUserData({
      ...userdata,
      [e.target.name]: e.target.value,
      createdBy: {
        userId: user.userId,
      },
    });
  };

  //! OnChange for File
  const onFileChange = (e) => {
    setFileName(e.target.files[0].name);
    setselectedFile(e.target.files[0]);
    if (!selectedFile) {
      alert("File is selected");
      return false;
    }
  };

//!USEEFFECT
  useEffect(() => {
    FetchData();
  }, []);

  //! File Upload
  const onFileUpload = (e) => {
    e.preventDefault();

    const data = new FormData();
    data.append("file", selectedFile);

    axios({
      origin: "*",
      method: "post",
      url: "https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/file/uploadFile",
      headers: {
        "content-type": "multipart/form-data",
        Authorization: "Bearer " + user.accessToken,
      },
      data,
    })
      .then(function (res) {
        console.log(res.data.fileName);
        setFileName(res.data.fileName);
        alert(res.data.message);
        setUserData({ ...userdata, fileName: fileName });
      })
      .catch(function (error) {
        console.log(error);
      });
  };



  //! validation

  const setField = (field) => {
    if (!!errors[field]) {
      setErrors({ ...errors, [field]: null });
    }
  };

  const validateForm = () => {
    const newErrors = {};

    if (!userdata.fullName || userdata.fullName === "") {
      newErrors.fullName = "enter the name";
    }
    if (!userdata.gender || userdata.gender === "") {
      newErrors.gender = "enter the gender";
    }
    if (!userdata.userName || userdata.userName === "") {
      newErrors.userName = "enter the userName";
    }
    if (!userdata.mailId || userdata.mailId === "") {
      newErrors.mailId = "enter the mail";
    }
    if (!userdata.otp || userdata.otp === "") {
      newErrors.otp = "enter the otp";
    }
    if (!userdata.password || userdata.password === "") {
      newErrors.password = "enter the password";
    }
    // if (!userdata.profilePicPath) {
    //   newErrors.profilePicPath = "Select a file";
    // } else {

    //   const maxFileSizeInBytes = 1 * 1024 * 1024;
    //   if (userdata.profilePicPath.size > maxFileSizeInBytes) {
    //     newErrors.profilePicPath = "File size exceeds the maximum allowed limit (1MB)";
    //   }
    // }

    return newErrors;
  };

  const postdata = async (e) => {
    // console.log("hi")
    e.preventDefault();
    console.log(headers, userdata);
    const formErrors = validateForm();

    if (Object.keys(formErrors).length > 0) {
      setErrors(formErrors);
    } else {
      await postCandiData(headers, userdata);

      console.log(userdata);
      setUserData({
        fullName: "",
        gender: "",
        mailId: "",
        userName: "",
        profilePicPath: "",
        otp: "",
        password: "",
      });
      setselectedFile(null);
      setFileName("");
    }
  };


//todo ==> GET  TOPIC DATA

const FetchData=async()=>{

let res= await fetchCandiData(headers);
console.log(res.data.content);

var fetchedcandidata=res.data.content
console.log(fetchedcandidata);

var tabledata=[];

// fetchedcandidata.map((can)=>{

//   tabledata.push({
//     mobileUserId:can.mobileUserId,
//     fullName:can.fullName,
//     gender:can.gender,
//     mailId:can.mailId,
//     mobileNumber:can.mobileNumber,
//     otp:can.otp,
//     password:can.password,
//     userName:can.fullName===null || can.fullName===""? "Anonymous":can.fullName,
//     profilePicPath:can.profilePicPath,
//     createdBy:can.createdBy.userName
//   })
// })
// console.log(tabledata);
// setCandidate(tabledata);
}








  return (
    <React.Fragment>
      <div className="pagetitle">
        <h1>Candidate</h1>
        <nav>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item active">Candidate</li>
          </ol>
        </nav>

        <Card>
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => {
                setKey(key);
              }}
              className="mb-3"
            >
              <Tab eventKey="Add" title="Add">
                <Form>
                  <Row className="mb-3">
                    <Col sm={4}>
                      <Form.Group className="mb-3" controlId="formGridFullName">
                        <Form.Label>Full Name</Form.Label>
                        <Form.Control
                          type="text"
                          name="fullName"
                          value={userdata.fullName}
                          placeholder="Enter Full Name"
                          onChange={(e) => {
                            changehandler(e);
                            setField("fullName");
                          }}
                          isInvalid={!!errors.fullName}
                        />
                        <Form.Control.Feedback type="invalid">
                          {errors.fullName}
                        </Form.Control.Feedback>
                      </Form.Group>
                    </Col>

                    <Col sm={4}>
                      <Form.Group className="mb-3" controlId="formGridGender">
                        <Form.Label>Gender</Form.Label>
                        <Form.Control
                          type="text"
                          name="gender"
                          value={userdata.gender}
                          placeholder="Enter Gender"
                          onChange={(e) => {
                            changehandler(e);
                            setField("gender");
                          }}
                          isInvalid={!!errors.gender}
                        />
                        <Form.Control.Feedback type="invalid">
                          {errors.gender}
                        </Form.Control.Feedback>
                      </Form.Group>
                    </Col>

                    <Col sm={4}>
                      <Form.Group className="mb-3" controlId="formGridAddress">
                        <Form.Label>userName</Form.Label>
                        <Form.Control
                          type="text"
                          name="userName"
                          value={userdata.userName}
                          placeholder="enter the username"
                          onChange={(e) => {
                            changehandler(e);
                            setField("userName");
                          }}
                          isInvalid={!!errors.userName}
                        />
                        <Form.Control.Feedback type="invalid">
                          {errors.userName}
                        </Form.Control.Feedback>
                      </Form.Group>
                    </Col>
                  </Row>

                  <Row className="mb-3">
                    <Form.Group as={Col} controlId="formGridCity">
                      <Form.Label>mailId</Form.Label>
                      <Form.Control
                        type="text"
                        name="mailId"
                        value={userdata.mailId}
                        placeholder="enter the mail id"
                        onChange={(e) => {
                          changehandler(e);
                          setField("mailId");
                        }}
                        isInvalid={!!errors.mailId}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.mailId}
                      </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridState">
                      <Form.Label>OTP</Form.Label>
                      <Form.Control
                        type="text"
                        name="otp"
                        value={userdata.otp}
                        placeholder="enter the otp"
                        onChange={(e) => {
                          changehandler(e);
                          setField("otp");
                        }}
                        isInvalid={!!errors.otp}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.otp}
                      </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridZip">
                      <Form.Label>Password</Form.Label>
                      <Form.Control
                        type="text"
                        name="password"
                        value={userdata.password}
                        placeholder="enter the password"
                        onChange={(e) => {
                          changehandler(e);
                          setField("password");
                        }}
                        isInvalid={!!errors.password}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.password}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Row>

                  <Row className="align-items-center">
                    <Col>
                      <Form.Group className="position-relative mb-3">
                        <Form.Label>File</Form.Label>
                        <Form.Control
                          type="file"
                          required
                          name="profilePicPath"
                          placeholder="please select the file"
                          accept="accept='.doc,.docx,application/pdf"
                          ref={inputRef}
                          // value={userdata.profilePicPath}
                          onChange={(e) => {
                            onFileChange(e);
                            setField("profilePicPath");
                          }}
                          isInvalid={!!errors.profilePicPath}
                        />
                        <Form.Control.Feedback type="invalid">
                          {errors.profilePicPath}
                        </Form.Control.Feedback>
                      </Form.Group>
                    </Col>
                    <Col>
                      <Button
                        variant="primary"
                        onClick={(e) => onFileUpload(e)}
                        type="submit"
                      >
                        Upload
                      </Button>
                    </Col>
                  </Row>

                  <Button variant="primary" type="submit" onClick={postdata}>
                    Submit
                  </Button>
                </Form>
              </Tab>
              <Tab eventKey="list-view" title="List">
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>FULL Name</th>
                      <th>GENDER</th>
                      <th>UserName</th>
                      <th>MAIL ID</th>
                      <th>OTP</th>
                      <th>PASSWORD</th>
                      <th>FILE</th>
                      <th>CRAEDTED DATE</th>
                    </tr>
                  </thead>
                  <tbody>
                    {/* {
                      candidate.map((c)=>{
                        return(
                          <tr>
                            <td>{c.mobileUserId}</td>
                            <td>{c.fullName}</td>
                            <td>{c.gender}</td>
                            <td>{c.userName}</td>
                            <td>{c.mailId}</td>
                            <td>{c.otp}</td>
                            <td>{c.password}</td>
                            <td>{c.profilePicPath}</td>
                            <td>{c.createdBy}</td>
                          </tr>
                        )
                      })
                    } */}
                  </tbody>
                </Table>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </div>
    </React.Fragment>
  );
};

export default Candidate;
