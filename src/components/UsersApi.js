import axios from "axios";



//todo ==> GET ROLES (DROPDOWN)
export const getRoles=async(headers)=>{
    
    return await axios({
        method: 'GET',
        url:"https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/role/v1/getAllRoles",
        headers:headers,
    })
}



// //todo ==> POST USER DATA
export const  postUsersData=async(headers,usedata)=>{
    try{
         await axios({
            method:"POST",
            url:"https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/userprofile/v1/createUserProfile",
            headers:headers,
            data:usedata
        }).then(function(res){
            console.log(res);
            if(res.data.responseCode){
                alert("UserProfile Successfully Created")
            }else {
                alert(res)
            }
        })
    }catch(error){
        alert(error);
    }
    
}

//todo ==> GET USER DATA
export const fetchUser=async(headers,usedata)=>{
    return await axios({
        method:"GET",
        url:"https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/userprofile/v1/getAllUserProfileByPagination/{pageNumber}/{pageSize}?pageNumber=0&pageSize=10",
        headers:headers,
        data:JSON.stringify(usedata)
    })
}

 //todo ==> GET DATA BY USERS ID
 export const getUserById=async(headers,id)=>{
    return await axios({
        method:"GET",
        url:`https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/userprofile/v1/getUserProfileByUserId/{userId}?userId=${id}`,
        headers:headers
    })
 }

  // todo==> UPDATE USER DATA
  export const updatedUser=async(headers,updatedata)=>{
    return await axios({
        method: 'PUT',
        url:"https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/userprofile/v1/updateUserProfile",
        headers:headers,
        data:JSON.stringify(updatedata)
      }).then(function (res) {
        console.log(res);
      if (res.data.responseCode === 201) {
        alert(res.data.message);
      } else if (res.data.responseCode === 400) {
        alert(res.data.errorMessage);
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  //todo ==> DELETE  USER DATA
export const deleteUser = async (id,headers) => {
    return await axios({
        method: "delete",
        url: `https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/topic/v1/deleteTopicById/${id}`,
        headers,
      }).then((res) => {
        if (res.data.responseCode === 200) {
          alert(res.data.message);
        } else if (res.data.responseCode === 400) {
          alert(res.data.errorMessage);
        }
      
      })
      .catch((err) => {
        console.log(err);
      });
}