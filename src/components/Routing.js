import Banner from "./Banner";
import Candidate from "./Candidate";
import Category from "./Category";
import Inventory from "./Inventory";
import Notification from "./Notification";
import Products from "./Products";
import Promo from "./Promo";
import Setting from "./Setting";
import Featured from "./Featured";


const Routing=[

      {
        path:"/dash/promo",
        name:"promo",
        element:<Promo/>
      },

      {
        path:"/dash/catg",
        name:"category",
        element:<Category/>
      },

      {
        path:"/dash/fetured",
        name:"users",
        element:<Featured />
      },
      {
        path:"/dash/ban",
        name:"Banner",
        element:<Banner/>
      },
      {
        path:"/dash/prod",
        name:"Product",
        element:<Products/>
      },
      {
        path:"/dash/inve",
        name:"Inventory",
        element:<Inventory/>
      },
      {
        path:"/dash/noti",
        name:"Notification",
        element:<Notification/>
      },
      {
        path:"/dash/sett",
        name:"Setting",
        element:<Setting/>
      },
      // {
      //   path:"/dash/candi",
      //   name:"candidate",
      //   element:<Candidate/>
      // },


]

export default Routing;