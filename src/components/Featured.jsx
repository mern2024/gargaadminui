import React, { useState, useEffect } from "react";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { Modal, Button, Form } from "react-bootstrap";
import baseUrl from "./BaseUrl"; // Adjust the import path based on your project structure

const Featured = () => {
  const [featuredItems, setFeaturedItems] = useState([]);
  const [newFeaturedItem, setNewFeaturedItem] = useState("");
  const [editFeaturedItem, setEditFeaturedItem] = useState("");
  const [editDescription, setEditDescription] = useState("");
  const [editFeaturedItemId, setEditFeaturedItemId] = useState("");
  const [deleteFeaturedItemId, setDeleteFeaturedItemId] = useState("");
  const [showAddModal, setShowAddModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [imageFile, setImageFile] = useState(null); // State variable for uploaded image file

  useEffect(() => {
    fetchFeaturedItems();
  }, []);

  const fetchFeaturedItems = async () => {
    try {
      const response = await axios.get(`${baseUrl}/featured/`);
      setFeaturedItems(response.data);
    } catch (error) {
      console.error("Error fetching featured items:", error);
    }
  };

  const addFeaturedItem = async () => {
    try {
      const formData = new FormData();
      formData.append("title", newFeaturedItem);
      formData.append("description", editDescription);
      formData.append("image", imageFile); // Append image file to form data

      const response = await axios.post(`${baseUrl}/featured/`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });

      setFeaturedItems([...featuredItems, response.data]);
      setNewFeaturedItem("");
      setEditDescription("");
      setShowAddModal(false);
    } catch (error) {
      console.error("Error adding featured item:", error);
    }
  };

  const handleFileUpload = (e) => {
    setImageFile(e.target.files[0]);
  };

  const handleEdit = async (featuredItemId) => {
    try {
      const response = await axios.put(


        `${baseUrl}/featured/${featuredItemId}`,
        {
          title: editFeaturedItem,
          description: editDescription,
        }
      );
      const updatedFeaturedItems = featuredItems.map((item) =>
        item._id === featuredItemId ? response.data : item
      );
      setFeaturedItems(updatedFeaturedItems);
      setEditFeaturedItem("");
      setEditDescription("");
      setEditFeaturedItemId("");
      setShowEditModal(false);
    } catch (error) {
      console.error("Error editing featured item:", error);
    }
  };

  const handleDelete = async (deleteFeaturedItemId) => {
    try {
      await axios.delete(
        `${baseUrl}/featured/${deleteFeaturedItemId}`
      );
      const updatedFeaturedItems = featuredItems.filter(
        (item) => item._id !== deleteFeaturedItemId
      );
      setFeaturedItems(updatedFeaturedItems);
      setDeleteFeaturedItemId("");
    } catch (error) {
      console.error("Error deleting featured item:", error);
    }
  };

  return (
    <div className="container">
      <h2>Featured Items</h2>
      <Button variant="primary" onClick={() => setShowAddModal(true)}>
        Add Featured Item
      </Button>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {featuredItems.map((item) => (
            <tr key={item._id}>
              <td>{item._id}</td>
              <td>
                <div className="d-flex align-items-center">
                  <div className="rounded-circle overflow-hidden me-2" style={{ width: '50px', height: '50px' }}>
                    <img src={item.image} alt={item.name} style={{ width: '100%', height: '100%', objectFit: 'cover' }} />
                  </div>
                  <span>{item.name}</span>
                </div>
              </td>
              <td>{item.description}</td>
              <td>
                {/* <button
                  className="btn btn-sm btn-primary me-2"
                  onClick={() => {
                    setEditFeaturedItem(item.name);
                    setEditDescription(item.description);
                    setEditFeaturedItemId(item._id);
                    setShowEditModal(true);
                  }}
                >
                  <FontAwesomeIcon icon={faEdit} />
                </button> */}
                <button
                  className="btn btn-sm btn-danger"
                  onClick={() => handleDelete(item._id)}
                >
                  <FontAwesomeIcon icon={faTrashAlt} />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      {/* Add Featured Item Modal */}
      <Modal show={showAddModal} onHide={() => setShowAddModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Add Featured Item</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Control
            type="text"
            placeholder="Enter Featured Item Name"
            value={newFeaturedItem}
            onChange={(e) => setNewFeaturedItem(e.target.value)}
          />
          <Form.Control
            as="textarea"
            placeholder="Enter Description"
            value={editDescription}
            onChange={(e) => setEditDescription(e.target.value)}
          />
          <Form.Group controlId="formFile" className="mb-3">
            <Form.Label>Upload Image</Form.Label>
            <Form.Control type="file" onChange={(e) => handleFileUpload(e)} />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowAddModal(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={addFeaturedItem}>
            Add
          </Button>
        </Modal.Footer>
      </Modal>
      {/* Edit Featured Item Modal */}
      <Modal show={showEditModal} onHide={() => setShowEditModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Featured Item</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Control
            type="text"
            placeholder="Enter Featured Item Name"
            value={editFeaturedItem}
            onChange={(e) => setEditFeaturedItem(e.target.value)}
          />
          <Form.Control
            as="textarea"
            placeholder="Enter Description"
            value={editDescription}
            onChange={(e) => setEditDescription(e.target.value)}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowEditModal(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={() => handleEdit(editFeaturedItemId)}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Featured;
