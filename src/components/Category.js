import React, { useState, useEffect } from "react";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { Modal, Button, Form } from "react-bootstrap";
import baseUrl from "./BaseUrl"; // Adjust the import path based on your project structure

const Category = () => {
  const [categories, setCategories] = useState([]);
  const [newCategory, setNewCategory] = useState("");
  const [editCategory, setEditCategory] = useState("");
  const [editDescription, setEditDescription] = useState("");
  const [editCategoryId, setEditCategoryId] = useState("");
  const [deleteCategoryId, setDeleteCategoryId] = useState("");
  const [showAddModal, setShowAddModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [imageFile, setImageFile] = useState(null); // State variable for uploaded image file

  useEffect(() => {
    fetchCategories();
  }, []);

  const fetchCategories = async () => {
    try {
      const response = await axios.get(`${baseUrl}/categories/`);
      setCategories(response.data);
    } catch (error) {
      console.error("Error fetching categories:", error);
    }
  };

  const addCategory = async () => {
    try {
      const formData = new FormData();
      formData.append("name", newCategory);
      formData.append("description", editDescription);
      formData.append("image", imageFile); // Append image file to form data
      console.log("Form data: " + JSON.stringify(formData))

      const response = await axios.post(`${baseUrl}/categories/`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });

      setCategories([...categories, response.data]);
      setNewCategory("");
      setEditDescription("");
      setShowAddModal(false);
    } catch (error) {
      console.error("Error adding category:", error);
    }
  };

  const handleFileUpload = (e) => {
    setImageFile(e.target.files[0]);
  };

  const handleEdit = async (categoryId) => {
    try {
      const response = await axios.put(
        `${baseUrl}/categories/${categoryId}`,
        {
          name: editCategory,
          description: editDescription,
        }
      );
      const updatedCategories = categories.map((cat) =>
        cat._id === categoryId ? response.data : cat
      );
      setCategories(updatedCategories);
      setEditCategory("");
      setEditDescription("");
      setEditCategoryId("");
      setShowEditModal(false);
    } catch (error) {
      console.error("Error editing category:", error);
    }
  };

  const handleDelete = async (deleteCategoryId) => {
    try {
      await axios.delete(
        `${baseUrl}/categories/${deleteCategoryId}`
      );
      const updatedCategories = categories.filter(
        (cat) => cat._id !== deleteCategoryId
      );
      setCategories(updatedCategories);
      setDeleteCategoryId("");
    } catch (error) {
      console.error("Error deleting category:", error);
    }
  };

  return (
    <div className="container">
      <h2>Categories</h2>
      <Button variant="primary" onClick={() => setShowAddModal(true)}>
        Add Category
      </Button>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        {categories.map((category) => (
  <tr key={category._id}>
    <td>{category._id}</td>
    <td>
      <div className="d-flex align-items-center">
        <div className="rounded-circle overflow-hidden me-2" style={{ width: '50px', height: '50px' }}>
          <img src={category.image} alt={category.name} style={{ width: '100%', height: '100%', objectFit: 'cover' }} />
        </div>
        <span>{category.name}</span>
      </div>
    </td>
    <td>{category.description}</td>
    <td>
      <button
        className="btn btn-sm btn-primary me-2"
        onClick={() => {
          setEditCategory(category.name);
          setEditDescription(category.description);
          setEditCategoryId(category._id);
          setShowEditModal(true);
        }}
      >
        <FontAwesomeIcon icon={faEdit} />
      </button>
      <button
        className="btn btn-sm btn-danger"
        onClick={() => handleDelete(category._id)}
      >
        <FontAwesomeIcon icon={faTrashAlt} />
      </button>
    </td>
  </tr>
))}

        </tbody>
      </table>
      {/* Add Category Modal */}
      <Modal show={showAddModal} onHide={() => setShowAddModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Add Category</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Control
            type="text"
            placeholder="Enter Category Name"
            value={newCategory}
            onChange={(e) => setNewCategory(e.target.value)}
          /><br></br>
          <Form.Control
            as="textarea"
            placeholder="Enter Description"
            value={editDescription}
            onChange={(e) => setEditDescription(e.target.value)}
          /><br></br>
          <Form.Group controlId="formFile" className="mb-3">
            <Form.Label>Upload Image</Form.Label>
            <Form.Control type="file" onChange={(e) => handleFileUpload(e)} />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowAddModal(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={addCategory}>
            Add
          </Button>
        </Modal.Footer>
      </Modal>
      {/* Edit Category Modal */}
      {/* Edit Category Modal */}
      <Modal show={showEditModal} onHide={() => setShowEditModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Category</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Control
            type="text"
            placeholder="Enter Category Name"
            value={editCategory}
            onChange={(e) => setEditCategory(e.target.value)}
          /> <br></br>
          <Form.Control
            as="textarea"
            placeholder="Enter Description"
            value={editDescription}
            onChange={(e) => setEditDescription(e.target.value)}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowEditModal(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={() => handleEdit(editCategoryId)}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Category;
