import React, { useEffect, useState } from "react";
import Card from "react-bootstrap/Card";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Table from "react-bootstrap/Table";
import {
  deletePromo,
  fetchPromo,
  getPromoById,
  postPromoData,
  updatedPromo,
} from "../PromoApi";
import moment from "moment";
import "../App.css"

const Promo = () => {
  const [userdata, setUserData] = useState({
    promoName: "",
    description: "",
    youTube: "",
  });
  const [errors, setErrors] = useState({});
  const [onEdit, setOnEdit] = useState(false);
  const [promo, setPromo] = useState([]);
  const [promoId, setrPromoId] = useState();
  const [key, setKey] = useState("list-view");

  //!Tokens and Headers
  const user = JSON.parse(sessionStorage.getItem("user"));
  // console.log(user)
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  //!USEEFFECT
  useEffect(() => {
    FetchData();
  }, [promo,key,promoId]);

  //!onchange for submit data
  const changehandler = (e) => {
    setUserData({
      ...userdata,
      [e.target.name]: e.target.value,
      createdBy: { userId: user.userId },
    });
  };

  


  //! Validation Form
  const setField = (field) => {
    if (!!errors[field]) {
      setErrors({ ...errors, [field]: null });
    }
  };

  const validateForm = () => {
    const newErrors = {};

    if (!userdata.promoName || userdata.promoName === "") {
      newErrors.promoName = "promoname cannot be empty";
    }

    if (!userdata.description || userdata.description === "") {
      newErrors.description = "description cannot be empty";
    }
    if (!userdata.youTube || userdata.youTube === "") {
      newErrors.youTube = "description cannot be empty";
    }

    return newErrors;
  };


  //todo ==> GET DATA BY PROMO ID
const handleEdit=async(id)=>{
//  console.log(id);
var res= await getPromoById(headers,id)
console.log(res)

let det=res.data
setrPromoId(det.promoId)
setUserData({promoName:det.promoName,description:det.description,youTube:det.youTube})
setKey("Add")
setOnEdit(true)
}


// todo==> UPDATE PROMO
const updatedata=async(e)=>{
  e.preventDefault();
//  console.log(userdata);
 console.log(promoId);
 var updateddata={...userdata,promoId,updatedBy:{userId:user.userId}}
//  console.log(updateddata)
const  resp= await updatedPromo(headers,updateddata)
console.log(resp);
setKey("list-view")
FetchData();
 }

//todo ==> DELETE  PROMO DATA
 const handleDelete=async(id) => {
// console.log(id);
 await deletePromo(headers,id)
 FetchData();
 }




  //todo ==> POST PROMO DATA
  const postdata = (e) => {
    e.preventDefault();
    const formErrros = validateForm();
    if (Object.keys(formErrros).length > 0) {
      setErrors(formErrros);
    } else {
      console.log(userdata);
      // console.log(headers);

      postPromoData(userdata, headers);
      setUserData({ promoName: "", description: "", youTube: "" });
      setKey("list-view");
      setOnEdit(false);
      
    }
  };


  //todo ==> GET  PROMO DATA
  const FetchData=async()=>{
    // console.log("hi")
    let res= await fetchPromo(headers);
    // console.log(res.data.content)
    var fetchedData=res.data.content
    // console.log(fetchedData);

    var tabledata=[]
    fetchedData.map((p)=>{

      tabledata.push({
        promoId:p.promoId,
        promoName:p.promoName,
        description:p.description,
        youTube:<iframe width="90" height="80" src={`https://www.youtube.com/embed/${p.youTube}`}>
        </iframe>,
        insertedDate:moment(p.insertedDate).format('L'),
        createdBy:(p.createdBy.userName===null)?"no name":p.createdBy.userName,
      })
    })
    // console.log(tabledata);
    setPromo(tabledata);
  }
 

  return (
    <React.Fragment>
      {/* braedcrum strating */}
      <div className="pagetitle">
        <h1>Promo</h1>
        <nav>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item active">Promo</li>
          </ol>
        </nav>
        {/* ending breadcrum */}

        <Card >
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => {
                setKey(key);
              }}
              
            >
              <Tab eventKey="Add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                  <Form.Group className="mb-3" controlId="formPromoName">
                    <Form.Label>Promo Name</Form.Label>
                    <Form.Control
                      type="promoName"
                      name="promoName"
                      value={userdata.promoName}
                      placeholder="Enter name"
                      className="w-50"
                      onChange={(e) => {
                        changehandler(e);
                        setField("promoName");
                      }}
                      isInvalid={!!errors.promoName}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.promoName}
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group
                    className="mb-3"
                    controlId="exampleForm.Descrpition"
                  >
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      name="description"
                      value={userdata.description}
                      rows={3}
                      placeholder="Enter Description"
                      className="w-75"
                      onChange={(e) => {
                        changehandler(e);
                        setField("description");
                      }}
                      isInvalid={!!errors.description}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.description}
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formYouTubeLink">
                    <Form.Label>YouTube Link</Form.Label>
                    <Form.Control
                      type="youTube"
                      name="youTube"
                      value={userdata.youTube}
                      placeholder="YoutubeLink"
                      className="w-80"
                      onChange={(e) => {
                        changehandler(e);
                        setField("ylink");
                      }}
                      isInvalid={!!errors.youTube}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.youTube}
                    </Form.Control.Feedback>
                  </Form.Group>
                  {onEdit ? (
                    <Button
                      variant="primary"
                      type="submit"
                      onClick={(e)=>{updatedata(e)}}
                    >
                      Update
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      type="submit"
                      onClick={(e) => {
                        postdata(e);
                      }}
                    >
                      Submit
                    </Button>
                  )}
                </Form>
              </Tab>
              <Tab eventKey="list-view" title="List">
              <div className="table-responsive-container">
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Promo Name</th>
                      <th>Description</th>
                      <th>YouTube</th>
                      <th>Created By</th>
                      <th>Inserted Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   {promo.map((p)=>{
                    return(
                      <tr>
                        <td>{p.promoId}</td>
                        <td>{p.promoName}</td>
                        <td>{p.description}</td>
                        <td>{p.youTube}</td>
                        <td>{p.createdBy}</td>
                        <td>{p.insertedDate}</td>
                        <td><Button  variant="success" className="m-2" onClick={()=>{handleEdit(p.promoId)}}>Edit</Button>
                        <Button variant="danger" onClick={()=>{handleDelete(p.promoId)}}>Delete</Button>
                        </td>
                      </tr>
                    )
                   })}
                  </tbody>
           
                </Table>
                </div>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </div>
    </React.Fragment>
  );
};

export default Promo;
