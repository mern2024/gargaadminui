import React, { useEffect, useRef, useState } from "react";
import {
  Button,
  Card,
  Col,
  Container,
  Form,
  Row,
  Tab,
  Table,
  Tabs,
} from "react-bootstrap";
import Select from "react-select";
import axios from "axios";
import moment from "moment/moment";
import { addAdvertise, deleteAdvertise, fetchAdvertise, getAdvertiseById, updatedAdvertise } from "./BannerApi";
import "../App.css";





const Banner = () => {
  const [userdata, setUserData] = useState({
    advertisementName: "",
    description: "",
  });
  const [errors, setErrors] = useState({});
  const [onEdit, setOnEdit] = useState(false);
  const [advertisement, setAdvertisement] = useState([]);
  const [advertisementId, setAdvertisementId] = useState("");
  const [keys, setKeys] = useState("list-view");
  //!State Initialzation For FILE UPLOAD
  const [fileName, setFileName] = useState("");
  const [selectedFile, setselectedFile] = useState("");
  const inputRef = useRef(null);

  //!Tokens and Headers
  const user = JSON.parse(sessionStorage.getItem("user"));
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  const ImageUrl = "https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/file/downloadFile/?filePath=";

  //!onchange for submit data
  const changehandler = (e) => {
    setUserData({
      ...userdata,
      [e.target.name]: e.target.value,
      createdBy: { userId: user.userId },
    });
  };

  //! validation

  const setFiled = (field) => {
    if (!!errors[field]) {
      setErrors({ ...errors, [field]: null });
    }
  };

  const validateForm = () => {
    const newErrors = {};

    if (!userdata.advertisementName || userdata.advertisementName === "") {
      newErrors.advertisementName = "please enter AdvertisementName";
    }

    if (!userdata.description || userdata.description === "") {
      newErrors.description = "please enter description";
    }

    return newErrors;
  };

  //!onchange for upload file
  const onFileChange = (e) => {
    setFileName(e.target.files[0].name);
    setselectedFile(e.target.files[0]);
    if (!selectedFile) {
      alert("image is selected");
      return false;
    }
  };


 //!USEEFFECT
 useEffect(() => {
  console.log(user.accessToken);
  FetchData();
 }, []);




  //!FILE UPLOAD
  const onFileUpload = async (e) => {
    e.preventDefault();

    console.log(fileName);
    const data = new FormData();
    data.append("file", selectedFile);
    setUserData({ ...userdata, fileName: fileName });

    await axios({
      origin: "*",
      method: "post",
      url: "https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/file/uploadFile",
      headers: {
        "content-type": "multipart/form-data",
        Authorization: "Bearer " + user.accessToken,
      },
      data,
    })
      .then(function (res) {
        setFileName(res.data.fileName);
        alert(res.data.message);
        setUserData({ ...userdata, fileName: fileName });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  //todo ==> POST PROMO DATA
  const postdata = (e) => {
    e.preventDefault();

    const formErrors = validateForm();

    if (Object.keys(formErrors).length > 0) {
      setErrors(formErrors);
    } else {
      addAdvertise(userdata, headers);
      console.log(userdata);
      console.log(headers);
      setUserData({ advertisementName: "", description: "" });
      inputRef.current.value = null;
      setKeys("list-view");
    }
  };


  // todo ==> GET LANGUAGE DATA
  const FetchData=async()=>{
    let response = await fetchAdvertise(headers);
         console.log(response.data.content);
         var bdata=response.data.content;
        var tableArry=[];
       bdata.map((b)=>{
      // console.log(b);
        tableArry.push({
        advertisementId:b.advertisementId,
        advertisementName:b.advertisementName,
        description:b.description,
        file:(b.filePath===null) ? "NO IMAGE FOUND":<img src={ImageUrl + b.filePath} alt={b.fileName} style={{ width: 100, height: 50 }}/>,
        createdBy:(b.createdBy ===null ) ? "No Creator":b.createdBy.userName,
        createdBy:(b.createdBy ===null ) ? "No Creator":b.createdBy.userName,
        insertedDate:moment(b.insertedDate).format("L"),
        updatedBy:(b.updatedBy ===null ) ? "No Updator":b.updatedBy.userName,
        updatedDate:moment(b.updatedBatch).format("L")});
      });
     
      console.log(tableArry);
      setAdvertisement(tableArry);
  }

// todo==> GET ADVERTISEMENT BY ID
const handleEdit = async(advertisementId) => {
  console.log(advertisementId);
  let res = await getAdvertiseById(advertisementId,headers);
  let det = res.data;
  console.log(det);

  setAdvertisementId(det.advertisementId);
  setUserData({
        advertisementName: det.advertisementName,
        description: det.description,
        fileName:det.fileName
      });
      setOnEdit(true);
      setKeys("add");
      FetchData();
 }


// todo==> UPDATE ADVERTISEMENT
const updatedata = async(e) => {
 
  e.preventDefault();
  var updatedData = {...userdata,advertisementId,updatedBy:{userId:user.userId}} ;
  console.log(updatedData);
 
  const res = await updatedAdvertise(updatedData,headers);

   setUserData({advertisementName:"",description:"",fileName:""});
  setOnEdit(false);
  setKeys("list-view");
  FetchData();
}



// todo ==> DELETE LANGUAGE DATA
const handleDelete = async(advertisementId) => {
  await deleteAdvertise(advertisementId,headers);
  FetchData();
 };





  return (
    <React.Fragment>
      {/* braedcrum strating */}
      <div className="pagetitle">
        <h1>Banner</h1>
        <nav>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item active">Banner</li>
          </ol>
        </nav>
        {/* ending breadcrum */}

        <Card >
          <Card.Body>
            <Tabs
              activeKey={keys}
              onSelect={(keys) => {
                setKeys(keys);
              }}
              className="mb-3"
            >
              <Tab eventKey="Add" title={onEdit ? "Edit" : "Add"}>
                <Form>
                  <Form.Group className="mb-3 col-6">
                    <Form.Label>Advertisement Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Advertisement Name"
                      name="advertisementName"
                      value={userdata.advertisementName}
                      onChange={(e) => {
                        changehandler(e);
                        setFiled("advertisementName");
                      }}
                      isInvalid={!!errors.advertisementName}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.advertisementName}
                    </Form.Control.Feedback>
                  </Form.Group>
                  <Form.Group
                    className="mb-3 "
                    controlId="exampleForm.ControlTextarea1"
                  >
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      name="description"
                      value={userdata.description}
                      className="w-40"
                      placeholder="enter the description"
                      onChange={(e) => {
                        changehandler(e);
                        setFiled("description");
                      }}
                      isInvalid={!!errors.description}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.description}
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Row className="align-items-center">
                    <Col>
                      <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Select File</Form.Label>
                        <Form.Control
                          type="file"
                          onChange={(e) => onFileChange(e)}
                          accept=".pdf, .jpg, .png"
                          ref={inputRef}
                        />
                      </Form.Group>
                    </Col>
                    <Col className="pt-3">
                      <Button
                        variant="primary"
                        type="submit"
                        onClick={onFileUpload}
                      >
                        Upload
                      </Button>
                    </Col>
                  </Row>
                {onEdit?<Button
                      variant="primary"
                      type="submit"
                      onClick={(e)=>{updatedata(e)}}
                    >
                      Update
                    </Button> :<Button
                      variant="primary"
                      type="submit"
                      onClick={(e) => {
                        postdata(e);
                      }}
                    >
                      Submit
                    </Button>}
                    
                </Form>
              </Tab>
              <Tab eventKey="list-view" title="List">
              <div className="table-responsive-container">
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>DESCRIPTION</th>
                      <th>File</th>
                      <th>Created By</th>
                      <th>Updated By</th>
                      <th>Inserted Date</th>
                      <th>Updated Date</th>
                      <th>ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      advertisement.map((a)=>{
                        return (
                          <tr>
                            <td>{a.advertisementId}</td>
                            <td>{a.advertisementName}</td>
                            <td>{a.description}</td>
                            <td>{a.file}</td>
                            <td>{a.createdBy}</td>
                            <td>{a.updatedBy}</td>
                            <td>{a.insertedDate}</td>
                            <td>{a.updatedDate}</td>
                            <td><Button  variant="success" className="m-2" onClick={()=>{handleEdit(a.advertisementId)}}>Edit</Button>
                        <Button variant="danger" onClick={()=>{handleDelete(a.advertisementId)}}>Delete</Button></td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </Table>
                </div>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </div>
    </React.Fragment>
  );
};

export default Banner;
