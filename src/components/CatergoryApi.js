import axios from "axios";

//todo ==> POST CATEGORY DATA

export const postCategoryData = (cdata, headers) => {
  // console.log(cdata)
  try {
    axios({
      method: "POST",
      url: "https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/category/v1/createCategory",
      headers: headers,
      data: JSON.stringify(cdata),
    }).then(function (res) {
      console.log(res);
      if (res.data.responseCode === 201) {
        alert("Category Successfully Created");
      } else if (res.data.responseCode === 400) {
        alert(res.data.errorMessage);
      }
    });
  } catch (error) {
    alert(error);
  }
};


//todo ==> GET  CATEGORY DATA

export const fetchCatergoryData =async(headers) =>{
       return await axios({
        method:"GET",
        url:"https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/category/v1/getAllCategoryByPagination/{pageNumber}/{pageSize}?pageNumber=0&pageSize=10",
        headers:headers
      })

}

//todo ==> GET DATA BY  CATEGORY ID

export const getcategoryById=async(headers,id)=>{
  return await axios({
    method:"GET",
    url:`https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/category/v1/getCategoryByCategoryId/{categoryId}?categoryId=${id}`,
    headers:headers,
  })
}

 // todo==> UPDATE  CATEGORY
 export const updatedCategory=async(headers,updateddata)=>{
    await axios({
    method: 'PUT',
    url:"https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/category/v1/updateCategory",
    headers:headers,
    data:JSON.stringify(updateddata)
   }).then(function(res){
    if(res.data.responseCode===201){
      alert("Category Successfully updated")
    }else if(res.data.responseCode===400){
      alert(res.data.errorMessage);
    }
   })
 }


 //todo ==> DELETE  CATEGORY DATA
 export const deleteCategory=(headers,id)=>{
  axios({
    method:"DELETE",
    url:`https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/category/v1/deleteCategoryById/${id}`,
    headers:headers,
  }).then((res) => {
    if (res.data.responseCode === 200) {
      alert(res.data.message);
    } else if (res.data.responseCode === 400) {
      alert(res.data.errorMessage);
    }
  })
  .catch((err) => {
    console.log(err);
  });
 }