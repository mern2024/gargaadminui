import React, { useState, useEffect } from "react";
import axios from "axios";
import { Modal, Button, Form } from "react-bootstrap";
import baseUrl from "./BaseUrl"; // Adjust the import path based on your project structure
import 'bootstrap/dist/css/bootstrap.min.css';

const Product = () => {
  const [products, setProducts] = useState([]);
  const [categories, setCategories] = useState([]);
  const [selectedImages, setSelectedImages] = useState([]);
  const [newProduct, setNewProduct] = useState({
    productName: "",
    description: "",
    category: "",
    size: "",
    wood: "",
    finish: "",
    price: "",
    features: "",
    legType: "",
    glassType: ""
  });
  const [editProduct, setEditProduct] = useState({
    _id: "",
    productName: "",
    description: "",
    category: "",
    size: "",
    wood: "",
    finish: "",
    price: "",
    features: "",
    legType: "",
    glassType: ""
  });
  const [showAddModal, setShowAddModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);

  useEffect(() => {
    fetchProducts();
    fetchCategories();
  }, []);

  const fetchProducts = async () => {
    try {
      const response = await axios.get(`${baseUrl}/products/`);
      setProducts(response.data);
    } catch (error) {
      console.error("Error fetching products:", error);
    }
  };

  const fetchCategories = async () => {
    try {
      const response = await axios.get(`${baseUrl}/categories/`);
      setCategories(response.data);
    } catch (error) {
      console.error("Error fetching categories:", error);
    }
  };

  const addProduct = async () => {
    try {
      const formData = new FormData();
      formData.append("productName", newProduct.productName);
      formData.append("description", newProduct.description);
      formData.append("category", newProduct.category);
      formData.append("size", newProduct.size);
      formData.append("wood", newProduct.wood);
      formData.append("finish", newProduct.finish);
      formData.append("price", newProduct.price);
      formData.append("features", newProduct.features);
      formData.append("legType", newProduct.legType);
      formData.append("glassType", newProduct.glassType);
      selectedImages.forEach((image) => {
        formData.append("images", image);
      });

      const response = await axios.post(
        `${baseUrl}/products/`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );

      setProducts([...products, response.data]);
      setNewProduct({
        productName: "",
        description: "",
        category: "",
        size: "",
        wood: "",
        finish: "",
        price: "",
        features: "",
        legType: "",
        glassType: ""
      });
      setSelectedImages([]);
      setShowAddModal(false);
    } catch (error) {
      console.error("Error adding product:", error);
    }
  };

  const handleEdit = async () => {
    try {
      console.log("while editing data"+JSON.stringify(editProduct))
      const formData = new FormData();
      formData.append("productName", editProduct.productName);
      formData.append("description", editProduct.description);
      formData.append("category", editProduct.category.name);
      formData.append("size", editProduct.size);
      formData.append("wood", editProduct.wood);
      formData.append("finish", editProduct.finish);
      formData.append("price", editProduct.price);
      formData.append("features", editProduct.features);
      formData.append("legType", editProduct.legType);
      formData.append("glassType", editProduct.glassType);
      editProduct.images.forEach((image) => {
        formData.append("images", image);
        console.log(formData)
      });
      console.log("while editing data"+JSON.stringify(formData))

      const response = await axios.put(
        `${baseUrl}/products/${editProduct._id}`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );

      console.log("From data: " + JSON.stringify(formData))

      const updatedProducts = products.map((prod) =>
        prod._id === editProduct._id ? response.data : prod
      );
      setProducts(updatedProducts);
      setEditProduct({
        _id: "",
        productName: "",
        description: "",
        category: "",
        size: "",
        wood: "",
        finish: "",
        price: "",
        features: "",
        legType: "",
        glassType: ""
      });
      setSelectedImages([]);
      setShowEditModal(false);
    } catch (error) {
      console.error("Error editing product:", error);
    }
  };

  const handleDelete = async (productId) => {
    const confirmDelete = window.confirm("Are you sure you want to delete this product?");
    if (confirmDelete) {
      try {
        await axios.delete(`${baseUrl}/products/${productId}`);
        const updatedProducts = products.filter(
          (product) => product._id !== productId
        );
        setProducts(updatedProducts);
      } catch (error) {
        console.error("Error deleting product:", error);
      }
    }
  };

  return (
    <div className="container">
      <Modal show={showAddModal} onHide={() => setShowAddModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="productName">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                value={newProduct.productName}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, productName: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="description">
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                value={newProduct.description}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, description: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="category">
              <Form.Label>Category</Form.Label>
              <Form.Control
                as="select"
                value={newProduct.category}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, category: e.target.value })
                }
              >
                <option value="">Select Category</option>
                {categories.map((cat) => (
                  <option key={cat._id} value={cat._id}>
                    {cat.name}
                  </option>
                ))}
              </Form.Control>
            </Form.Group>
            <Form.Group controlId="images">
              <Form.Label>Images</Form.Label>
              <Form.Control
                type="file"
                multiple
                onChange={(e) => setSelectedImages(Array.from(e.target.files))}
              />
            </Form.Group>
            {selectedImages.length > 0 && (
              <div>
                <h5>Selected Images:</h5>
                <ul>
                  {selectedImages.map((image, index) => (
                    <li key={index}>{image.name}</li>
                  ))}
                </ul>
              </div>
            )}
            <Form.Group controlId="size">
              <Form.Label>Product Size</Form.Label>
              <Form.Control
                type="text"
                value={newProduct.size}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, size: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="wood">
              <Form.Label>Wood Type</Form.Label>
              <Form.Control
                type="text"
                value={newProduct.wood}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, wood: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="finish">
              <Form.Label>Product Finish</Form.Label>
              <Form.Control
                type="text"
                value={newProduct.finish}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, finish: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="price">
              <Form.Label>Product Price</Form.Label>
              <Form.Control
                type="text"
                value={newProduct.price}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, price: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="features">
              <Form.Label>Product Features</Form.Label>
              <Form.Control
                type="text"
                value={newProduct.features}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, features: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="legType">
              <Form.Label>Leg Type</Form.Label>
              <Form.Control
                type="text"
                value={newProduct.legType}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, legType: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="glassType">
              <Form.Label>Glass Type</Form.Label>
              <Form.Control
                type="text"
                value={newProduct.glassType}
                onChange={(e) =>
                  setNewProduct({ ...newProduct, glassType: e.target.value })
                }
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowAddModal(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={addProduct}>
            Add Product
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showEditModal} onHide={() => setShowEditModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            {/* Use similar form fields for editing as used for adding */}
            <Form.Group controlId="productName">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                value={editProduct.productName}
                onChange={(e) =>
                  setEditProduct({ ...editProduct, productName: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="description">
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                value={editProduct.description}
                onChange={(e) =>
                  setEditProduct({ ...editProduct, description: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="category">
              <Form.Label>Category</Form.Label>
              <Form.Control
                as="select"
                value={editProduct.category}
                onChange={(e) =>
                  setEditProduct({ ...editProduct, category: e.target.value })
                }
              >
                <option value="">Select Category</option>
                {categories.map((cat) => (
                  <option key={cat._id} value={cat._id}>
                    {cat.name}
                  </option>
                ))}
              </Form.Control>
            </Form.Group>
            <Form.Group controlId="images">
              <Form.Label>Images</Form.Label>
              <Form.Control
                type="file"
                multiple
                onChange={(e) => setSelectedImages(Array.from(e.target.files))}
              />
            </Form.Group>
            {selectedImages.length > 0 && (
              <div>
                <h5>Selected Images:</h5>
                <ul>
                  {selectedImages.map((image, index) => (
                    <li key={index}>{image.name}</li>
                  ))}
                </ul>
              </div>
            )}
            <Form.Group controlId="size">
              <Form.Label>Product Size</Form.Label>
              <Form.Control
                type="text"
                value={editProduct.size}
                onChange={(e) =>
                  setEditProduct({ ...editProduct, size: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="wood">
              <Form.Label>Wood Type</Form.Label>
              <Form.Control
                type="text"
                value={editProduct.wood}
                onChange={(e) =>
                  setEditProduct({ ...editProduct, wood: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="finish">
              <Form.Label>Product Finish</Form.Label>
              <Form.Control
                type="text"
                value={editProduct.finish}
                onChange={(e) =>
                  setEditProduct({ ...editProduct, finish: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="price">
              <Form.Label>Product Price</Form.Label>
              <Form.Control
                type="text"
                value={editProduct.price}
                onChange={(e) =>
                  setEditProduct({ ...editProduct, price: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="features">
              <Form.Label>Product Features</Form.Label>
              <Form.Control
                type="text"
                value={editProduct.features}
                onChange={(e) =>
                  setEditProduct({ ...editProduct, features: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="legType">
              <Form.Label>Leg Type</Form.Label>
              <Form.Control
                type="text"
                value={editProduct.legType}
                onChange={(e) =>
                  setEditProduct({ ...editProduct, legType: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="glassType">
              <Form.Label>Glass Type</Form.Label>
              <Form.Control
                type="text"
                value={editProduct.glassType}
                onChange={(e) =>
                  setEditProduct({ ...editProduct, glassType: e.target.value })
                }
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowEditModal(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={handleEdit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>

      <h2>Product List</h2>
      <Button variant="primary" onClick={() => setShowAddModal(true)}>
        Add Product
      </Button>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Product Name</th>
            <th>Description</th>
            <th>Category</th>
            <th>Size</th>
            <th>Wood</th>
            <th>Finish</th>
            <th>Price</th>
            <th>Features</th>
            <th>Leg Type</th>
            <th>Glass Type</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => (
            <tr key={product._id}>
              <td>{product.productName}</td>
              <td><textarea rows={4} cols={40}>{product.description}</textarea></td>
              <td>{categories.find(cat => cat._id === product.category)?.name}</td>
              <td>{product.size}</td>
              <td>{product.wood}</td>
              <td>{product.finish}</td>
              <td>{product.price}</td>
              <td><textarea rows={4} cols={40}>{product.features}</textarea></td>
              <td>{product.legType}</td>
              <td>{product.glassType}</td>
              <td>
                 <Button
                  variant="info"
                  onClick={() => {
                    setEditProduct(product);
                    setShowEditModal(true);
                  }}
                >
                  Edit
                </Button> 
                <Button
                  variant="danger"
                  onClick={() => handleDelete(product._id)}
                >
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Product;
