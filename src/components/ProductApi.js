import axios from "axios";

//todo ==> GET ROLES (DROPDOWN)
export const getRoles = async (headers) => {
  return await axios({
    method: "GET",
    url: "https://virtullearning.cloudjiffy.net/ESevaAdmin/category/v1/queryAllCategory",
    headers,
    body: JSON.stringify(),
  });
};

//todo ==> POST PRODUCT DATA
export const addProdData = async (userdata, headers) => {
  try {
    await axios({
      method: "POST",
      url: "https://virtullearning.cloudjiffy.net/ESevaAdmin/product/v1/createProduct",
      headers: headers,
      data: JSON.stringify(userdata),
    }).then(function (res) {
      console.log(res);
      if (res.data.responseCode === 201) {
        alert("Product Successfully Created");
      } else if (res.data.responseCode === 400) {
        alert(res.data.errorMessage);
      }
    });
  } catch (error) {
    alert(error);
  }
};

//todo ==> GET  PRODUCT DATA
export const fetchProduct=async(headers)=>{
   return await axios({
        method:"GET",
        url:"https://virtullearning.cloudjiffy.net/ESevaAdmin/product/v1/getAllProductsByPagination/{pageNumber}/{pageSize}?pageNumber=0&pageSize=10",
        headers:headers
    })
}

//todo ==> GET DATA BY PRODUCT ID
export const  getProductById=async(id,headers) =>{
  return await axios({
    method:"GET",
    url:`https://virtullearning.cloudjiffy.net/ESevaAdmin/product/v1/getProductByProductId/{productId}?productId=${id}`,
    headers:headers
  })
}

 // todo==> UPDATE PRODUCTS
 export const updatedProduct=async(headers,updateddata)=>{
  await axios({
    method:"PUT",
    url:"https://virtullearning.cloudjiffy.net/ESevaAdmin/product/v1/updateProduct",
    headers:headers,
    data:updateddata
  }).then(function (res) {
    console.log(res);
  if (res.data.responseCode === 201) {
    alert(res.data.message);
  } else if (res.data.responseCode === 400) {
    alert(res.data.errorMessage);
  }
})
.catch(function (error) {
  console.log(error);
});
 }

 //todo ==> DELETE  PRODUCT DATA

 export const deleteProduct=async(headers,id)=>{
 await  axios({
    method:"DELETE",
    url:`https://virtullearning.cloudjiffy.net/ESevaAdmin/product/v1/deleteProductById/${id}`,
    headers:headers
  }).then((res) => {
    if (res.data.responseCode === 200) {
      alert(res.data.message);
    } else if (res.data.responseCode === 400) {
      alert(res.data.errorMessage);
    }
  })
  .catch((err) => {
    console.log(err);
  });
 }