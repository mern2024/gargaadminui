import React, { useState } from "react";
import { Link, Outlet } from "react-router-dom";
import image from "../images/userimg.png"
import axios from "axios";
import logo from "../images/logo.png";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const Dashboard = () => {
  const [sidebar, setSideBar] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");
  const [userInfo, setUserInfo] = useState();
  const[promocount,setPromoCount]=useState("");

 //!Tokens and Headers
 const user = JSON.parse(sessionStorage.getItem("user"));
 // console.log(user);
 const headers = {
   "Content-Type": "application/json",
   Authorization: "Bearer " + user.accessToken,
 };


 const navi=useNavigate();

// //!to fetch user name 
// const fetchUser=async()=>{
//   return await axios({
//     method:"GET",
//     url:`https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/login/v1/queryUserProfileByUserName/${user.userName}`,
//     headers,
//     body: JSON.stringify(),
//   }).then(function(res){
//     console.log(res.data.userName)
//     setUserInfo(res.data.userName)
//   })
//   .catch(function (error) {
//     console.log(error);
//   });
// }





 //!USEEFFECT
 useEffect(() => {
  //fetchUser();
  // getNotifications();
  // getNews();
  // getSuccess();
}, []);

//!LOGOUT FUNCTION
const Logout=(e)=>{
e.preventDefault();
navi("/")
sessionStorage.removeItem("user")
}




//!handle search
  const handleSearch = (e) => {
    e.preventDefault();
    console.log("Search query:", searchQuery);
    // Perform search or any desired action with the searchQuery
    // ...
  };


  return (
    <React.Fragment>
      <div>
        {/* ======= Header ======= */}
        <header
          id="header"
          className="header fixed-top d-flex align-items-center"
        >
          <div className="d-flex align-items-center justify-content-between">
            <Link to={"/dash"} className="logo d-flex align-items-center text-decoration-none">
            <img src={logo} alt="" />
              <span className="d-none d-lg-block " >GargaDecor</span>
            </Link>
            <i
              className="bi bi-list toggle-sidebar-btn"
              onClick={() => {
                setSideBar(!sidebar);
              }}
            />
          </div>
          {/* End Logo */}
          <div className="search-bar">
            <form
              className="search-form d-flex align-items-center"
              // method="POST"
              // action="#"
              onSubmit={handleSearch}
            >
              <input
                type="text"
                name="query"
                placeholder="Search"
                title="Enter search keyword"
                value={searchQuery}
                onChange={(e) => setSearchQuery(e.target.value)}
              />
              <button type="submit" title="Search">
                <i className="bi bi-search" />
              </button>
            </form>
          </div>
          {/* End Search Bar */}
          <nav className="header-nav ms-auto">
            <ul className="d-flex align-items-center">
              <li className="nav-item d-block d-lg-none">
                <a className="nav-link nav-icon search-bar-toggle " >
                  <i className="bi bi-search" />
                </a>
              </li>
              {/* End Search Icon*/}
              <li className="nav-item dropdown">
                <a
                  className="nav-link nav-icon"
                  href="#"
                  data-bs-toggle="dropdown"
                >
                  <i className="bi bi-bell" />
                  <span className="badge bg-primary badge-number">4</span>
                </a>
                {/* End Notification Icon */}
                <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow notifications">
                  <li className="dropdown-header">
                    You have 4 new notifications
                    <a href="#">
                      <span className="badge rounded-pill bg-primary p-2 ms-2">
                        View all
                      </span>
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li className="notification-item">
                    <i className="bi bi-exclamation-circle text-warning" />
                    <div>
                      <h4>Lorem Ipsum</h4>
                      <p>Quae dolorem earum veritatis oditseno</p>
                      <p>30 min. ago</p>
                    </div>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li className="notification-item">
                    <i className="bi bi-x-circle text-danger" />
                    <div>
                      <h4>Atque rerum nesciunt</h4>
                      <p>Quae dolorem earum veritatis oditseno</p>
                      <p>1 hr. ago</p>
                    </div>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li className="notification-item">
                    <i className="bi bi-check-circle text-success" />
                    <div>
                      <h4>Sit rerum fuga</h4>
                      <p>Quae dolorem earum veritatis oditseno</p>
                      <p>2 hrs. ago</p>
                    </div>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li className="notification-item">
                    <i className="bi bi-info-circle text-primary" />
                    <div>
                      <h4>Dicta reprehenderit</h4>
                      <p>Quae dolorem earum veritatis oditseno</p>
                      <p>4 hrs. ago</p>
                    </div>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li className="dropdown-footer">
                    <a href="#">Show all notifications</a>
                  </li>
                </ul>
                {/* End Notification Dropdown Items */}
              </li>
              {/* End Notification Nav */}
              <li className="nav-item dropdown">
                <a
                  className="nav-link nav-icon"
                  href="#"
                  data-bs-toggle="dropdown"
                >
                  <i className="bi bi-chat-left-text" />
                  <span className="badge bg-success badge-number">3</span>
                </a>
                {/* End Messages Icon */}
                <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow messages">
                  <li className="dropdown-header">
                    You have 3 new messages
                    <a href="#">
                      <span className="badge rounded-pill bg-primary p-2 ms-2">
                        View all
                      </span>
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li className="message-item">
                    <a href="#">
                      <img
                        src="assets/img/messages-1.jpg"
                        alt=""
                        className="rounded-circle"
                      />
                      <div>
                        <h4>Maria Hudson</h4>
                        <p>
                          Velit asperiores et ducimus soluta repudiandae labore
                          officia est ut...
                        </p>
                        <p>4 hrs. ago</p>
                      </div>
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li className="message-item">
                    <a href="#">
                      <img
                        src="assets/img/messages-2.jpg"
                        alt=""
                        className="rounded-circle"
                      />
                      <div>
                        <h4>Anna Nelson</h4>
                        <p>
                          Velit asperiores et ducimus soluta repudiandae labore
                          officia est ut...
                        </p>
                        <p>6 hrs. ago</p>
                      </div>
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li className="message-item">
                    <a href="#">
                      <img
                        src="assets/img/messages-3.jpg"
                        alt=""
                        className="rounded-circle"
                      />
                      <div>
                        <h4>David Muldon</h4>
                        <p>
                          Velit asperiores et ducimus soluta repudiandae labore
                          officia est ut...
                        </p>
                        <p>8 hrs. ago</p>
                      </div>
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li className="dropdown-footer">
                    <a href="#">Show all messages</a>
                  </li>
                </ul>
                {/* End Messages Dropdown Items */}
              </li>
              {/* End Messages Nav */}
              <li className="nav-item dropdown pe-3">
                <a
                  className="nav-link nav-profile d-flex align-items-center pe-0"
                  href="#"
                  data-bs-toggle="dropdown"
                >
                  <img
                    src={image}
                    alt="Profile"
                    className="rounded-circle"
                  />
                  <span className="d-none d-md-block dropdown-toggle ps-2">
                  {userInfo}
                  </span>
                </a>
                {/* End Profile Iamge Icon */}
                <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                  <li className="dropdown-header">
                  <h6>{userInfo}</h6>
                    <span>Web Designer</span>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li>
                    <a
                      className="dropdown-item d-flex align-items-center"
                      href="users-profile.html"
                    >
                      <i className="bi bi-person" />
                      <span>My Profile</span>
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li>
                    <a
                      className="dropdown-item d-flex align-items-center"
                      href="users-profile.html"
                    >
                      <i className="bi bi-gear" />
                      <span>Account Settings</span>
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li>
                    <a
                      className="dropdown-item d-flex align-items-center"
                      href="pages-faq.html"
                    >
                      <i className="bi bi-question-circle" />
                      <span>Need Help?</span>
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li>
                    <a
                      className="dropdown-item d-flex align-items-center"
                      href="#"
                    >
                      <i className="bi bi-box-arrow-right" 
                      onClick={(e)=>{Logout(e)}}
                       />
                      <span>Sign Out</span>
                    </a>
                  </li>
                </ul>
                {/* End Profile Dropdown Items */}
              </li>
              {/* End Profile Nav */}
            </ul>
          </nav>
          {/* End Icons Navigation */}
        </header>
        {/* End Header */}
        {/* ======= Sidebar ======= */}
        <div className={sidebar ? " " : "toggle-sidebar"}>
          <aside id="sidebar" class="sidebar">
            <ul class="sidebar-nav" id="sidebar-nav">
              <li class="nav-item">
                <Link class="nav-link " to="/dash">
                  <i class="bi bi-grid"></i>
                  <span>Dashboard</span>
                </Link>
              </li>
              {/* <!-- End Dashboard Nav --> */}

             
              {/* <!-- End Components Nav --> */}
              <li className="nav-item">
                <Link to="/dash/catg" className="nav-link collapsed">
                  <i className="bi bi-shop"></i>
                  <span>Category</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/dash/prod" className="nav-link collapsed">
                  <i className="bi bi-shop"></i>
                  <span>Products</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/dash/fetured" className="nav-link collapsed">
                  <i className="bi bi-shop"></i>
                  <span>Featured</span>
                </Link>
              </li>

             

            
              {/* <!-- End Charts Nav --> */}

              <li class="nav-heading">Pages</li>
              <li class="nav-item">
                <Link class="nav-link collapsed" to={"/"} >
                  <i class="bi bi-box-arrow-in-right" 
                  onClick={(e)=>{Logout(e)}}></i>
                  <span>Logout</span>
                </Link>
              </li>
              {/* <!-- End Login Page Nav --> */}
            </ul>
          </aside>

          {/* End Sidebar*/}
          <main id="main" className="main">
            {/* End Page Title */}
            <Outlet />
          </main>
          {/* End #main */}
          {/* ======= Footer ======= */}
          <footer id="footer" className="footer">
            <div className="copyright">
              © Copyright{" "}
              <strong>
                <span>Walkin Software Technologies</span>
              </strong>
              . All Rights Reserved
            </div>
            <div className="credits">
              Designed by <a href="https://saibiz.in/">Saibiz Innovations </a>
            </div>
          </footer>
          {/* End Footer */}
          <a
            href="#"
            className="back-to-top d-flex align-items-center justify-content-center"
          >
            <i className="bi bi-arrow-up-short" />
          </a>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Dashboard;
