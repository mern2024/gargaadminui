import React, { useEffect, useState } from "react";
import Card from "react-bootstrap/Card";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import { deleteNotofication, fetchNofication, getNotoficationById, postBulkNotification } from "./NotificationApi";
import moment from "moment";
import "../App.css"



const Notification = () => {
  const [userdata, setUserData] = useState({
    message: "",
    title: "",
    topic: "",
  });
  const [errors, setErrors] = useState({});

  //!ui state
  const [key, setKey] = useState("list-view");
  const [onEdit, setOnEdit] = useState(false);

  //! Bulk Notification data state

  const [bulkNotificationId, setBulkNotificationId] = useState("");
  const [bulkNotification, setBulkNotification] = useState([]);

  //!Tokens and Headers
  const user = JSON.parse(sessionStorage.getItem("user"));
  // console.log(user)
  const headers = {
    "Content-type": "application/json",
    Authorization: "Bearer " + user.accessToken,
  };

  //!USEEFFECT
  useEffect(() => {
    FetchData();
  }, []);

  //!onchange for submit data
  const changehandler = (e) => {
    setUserData({
      ...userdata,
      [e.target.name]: e.target.value,
      createdBy: { userId: user.userId },
    });
  };

  //! validation

  const setFiled = (field) => {
    if (!!errors[field]) {
      setErrors({ ...errors, [field]: null });
    }
  };

  const validateForm = () => {
    const newErrors = {};

    if (!userdata.message || userdata.message === "") {
      newErrors.message = "please enter  message";
    }

    if (!userdata.title || userdata.title === "") {
      newErrors.title = "please enter title";
    }

    if (!userdata.topic || userdata.topic === "") {
      newErrors.topic = "please enter topic";
    }

    return newErrors;
  };

  //todo ==> POST BULK NOTIFICATION DATA
  const postdata = async (e) => {
    e.preventDefault();

    const formErrors = validateForm();

    if (Object.keys(formErrors).length > 0) {
      setErrors(formErrors);
    } else {
      // console.log(userdata);
      await postBulkNotification(userdata, headers);
      setUserData({ message: "", title: "", topic: "" });
      setKey("list-view");
      setOnEdit(false);
    }
  };

  //todo ==> GET  NOTIFICATION DATA
  const FetchData = async () => {
    let res = await fetchNofication(headers);
    // console.log(res.data.content);

    var fetchedData = res.data.content;
    // console.log(fetchedData);

    var tabledata = [];
    fetchedData.map((b) => {
      tabledata.push({
        bulkNotificationId: b.bulkNotificationId,
        title: b.title,
        message: b.message,
        topic: b.topic,
        updatedBy: b.updatedBy.userName,
        insertedDate: moment(b.insertedDate).format("L"),
        updatedDate: moment(b.updatedDate).format("L"),
      });
    });
    setBulkNotification(tabledata);
  };

  //todo ==> GET DATA BY NOTIFICATION  ID

// const handleEdit=async(id)=>{
//   var res= getNotoficationById(headers,id)
//   console.log(res)
//   let det=res.data
//   setBulkNotificationId(det.bulkNotificationId)
//   setUserData({message:det.message,title:det.title,topic:det.topic})
//   setKey("Add")
// setOnEdit(true)
// }


// todo==> Delete NOTIFICATION
const handleDelete=async(id) => {

  await deleteNotofication(headers,id);
  FetchData();

}










  return (
    <>
      {/* braedcrum strating */}
      <div className="pagetitle">
        <h1>Notification</h1>
        <nav>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item active">Notification</li>
          </ol>
        </nav>
        {/* ending breadcrum */}

        <Card >
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => {
                setKey(key);
              }}
              className="mb-3"
            >
              <Tab eventKey="Add" title="Add">
                <Form>
                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Notification Title</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Add Title"
                      name="title"
                      value={userdata.title}
                      onChange={(e) => {
                        changehandler(e);
                        setFiled("title");
                      }}
                      isInvalid={!!errors.title}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.title}
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Message</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add Message"
                      name="message"
                      value={userdata.message}
                      onChange={(e) => {
                        changehandler(e);
                        setFiled("message");
                      }}
                      isInvalid={!!errors.message}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.message}
                    </Form.Control.Feedback>
                  </Form.Group>
                  <Form.Group className="mb-3 col-12">
                    <Form.Label>Topic</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Add Topic"
                      name="topic"
                      value={userdata.topic}
                      onChange={(e) => {
                        changehandler(e);
                        setFiled("topic");
                      }}
                      isInvalid={!!errors.topic}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.topic}
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Button
                    variant="primary"
                    type="submit"
                    onClick={(e) => {
                      postdata(e);
                    }}
                  >
                    Submit
                  </Button>
                </Form>
              </Tab>
              <Tab eventKey="list-view" title="list">
              <div className="table-responsive-container">
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Message</th>
                      <th>Topic</th>
                      <th>Created By</th>
                      <th>Updated By</th>
                      <th>Inserted Date</th>
                      <th>Updated Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    {bulkNotification.map((b) => {
                      return (
                        <tr>
                          <td>{b.bulkNotificationId}</td>
                          <td>{b.title}</td>
                          <td>{b.message}</td>
                          <td>{b.topic}</td>
                          <td>{b.updatedBy}</td>
                          <td>{b.topic}</td>
                          <td>{b.insertedDate}</td>
                          <td>{b.updatedDate}</td>
                          <td>
                            {/* <Button
                              variant="success"
                              className="m-2"
                              onClick={() => {
                                handleEdit(b.bulkNotificationId);
                              }}
                            >
                              Edit
                            </Button> */}
                            <Button
                              variant="danger"
                              onClick={() => {
                                handleDelete(b.bulkNotificationId);
                              }}
                            >
                              Delete
                            </Button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
                </div>
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </div>
    </>
  );
};

export default Notification;
