import React, { useEffect, useState } from "react";
import Card from "react-bootstrap/Card";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Table from "react-bootstrap/Table";
import {
  deleteUser,
  fetchUser,
  getRoles,
  getUserById,
  postUsersData,
  updatedUser,
 
} from "./UsersApi";
import Select from "react-select";
import moment from "moment";
import "../App.css"

const Users = () => {
  const [userdata, setUserData] = useState({
    fullName: "",
    mobileNumber: "",
    email: "",
    userName: "",
    password: "",
  });
  const [errors, setErrors] = useState({});
  const [key, setKey] = useState("list-view");
  const [usser, setUsser] = useState([]);
  //! initiazing state for roles
  const [roles, setRoles] = useState([]);
  const [roleId, setRoleId] = useState();
  const [roleValid, setRoleValid] = useState(true);
  const [selectedRole, setSelectedRole] = useState({
    value: "0",
    label: "Select...",
  });
  const [onedit, setOnEdit] = useState(false);
  const [categoryId, setCategoryId] = useState();

  //!Tokens and Headers
  const user = JSON.parse(sessionStorage.getItem("user"));
  // console.log(user);
  const headers = {
    "Content-Type": "application/json",
    Authorization: "Bearer" + " " + user.accessToken,
  };

  //! onChange Event To SET State
  const changehandler = (e) => {
    setUserData({
      ...userdata,
      [e.target.name]: e.target.value,
      createdBy: { userId: user.userId },
    });
  };

  //! OnChange for ROLE(DROPDOWN)
  const handleRoleChange = (value) => {
    setRoleValid(false);
    setSelectedRole(value);
    console.log(value.value);
    setUserData({
      ...userdata,
      roleDto: { roleId: value.value },
      createdBy: { userId: user.userId },
    });
    setRoleId(value.value);
  };

  //! Validation Form
  const setField = (field) => {
    if (!!errors[field]) {
      setErrors({ ...errors, [field]: null });
    }
  };

  const validateForm = () => {
    const newErrors = {};

    if (!userdata.fullName || userdata.fullName === "") {
      newErrors.fullName = "kindly enter fullName ";
    }

    if (!userdata.mobileNumber || userdata.mobileNumber === "") {
      newErrors.mobileNumber = "kindly enter number";
    } else {
      // Phone number validation pattern (example pattern)
      const phonePattern = /^\d{10}$/;

      // Check if the phone number matches the pattern
      if (!phonePattern.test(userdata.mobileNumber)) {
        newErrors.mobileNumber = "Invalid phone number";
      }
    }

    if (!userdata.email || userdata.email === "") {
      newErrors.email = "kindly enter email";
    }

    if (selectedRole.label === "Select.....") {
      newErrors.selectedRole = "Please select a role";
    }

    if (!userdata.userName || userdata.userName === "") {
      newErrors.userName = "kindly enter name";
    }

    if (!userdata.password || userdata.password === "") {
      newErrors.password = "kindly enter password";
    }

    return newErrors;
  };

  //!USEEFFCET
  useEffect(() => {
    FetchData();
    FetchRoles();
  }, [roleId, roleValid]);

  // todo==> GET ROLES(dropdown)
  const FetchRoles = async () => {
    let response = await getRoles(headers);
    // console.log(response.data);

    var down = response.data;
    var mdata = down.map((a) => {
      return { value: a.roleId, label: a.roleName };
    });
    // console.log(mdata);
    setRoles(mdata);
  };

  //todo ==> POST USER DATA
  const postdata = async (e) => {
    e.preventDefault();
    const formErrors = validateForm();
    if (Object.keys(formErrors).length > 0) {
      setErrors(formErrors);
    } else {
      await postUsersData(headers, userdata);
      // console.log(userdata);
      setSelectedRole({ value: "0", label: "Select....." });
      setRoleId(true);
      setRoleId("");
      setKey("list-view");
      setUserData({
        fullName: "",
        mobileNumber: "",
        email: "",
        userName: "",
        password: "",
      });
    }
  };

  //todo ==> GET USER DATA
  const FetchData = async () => {
    let res = await fetchUser(headers, userdata);
    // console.log(res.data.content);

    var fetcheddata = res.data.content;
    // console.log(res.data.content);

    const tabledata = [];

    fetcheddata.map((u) => {
      tabledata.push({
        userId: u.userId,
        fullName: u.fullName === null ? "no name" : u.fullName,
        userName: u.userName,
        mobileNumber: u.mobileNumber,
        email: u.email,
        password: u.password,
        roleDto: u.roleDto.roleName,
        updatedDate: moment(u.updatedDate).format("L"),
      });
    });
    // console.log(tabledata);
    setUsser(tabledata);
  };

  //todo ==> GET DATA BY USERS ID

  const handleEdit = async (id) => {
    console.log(id);

    var res = await getUserById(headers, id);
    // console.log(res.data);

    let det = res.data;
    console.log(det);
    setCategoryId(det.userId);
    setUserData({
      fullName: det.fullName,
      mobileNumber: det.mobileNumber,
      email: det.email,
      userName: det.userName,
      password: det.password === null ? "No Password" : det.password,
      roleDto: det.roleDto.roleName,
    });
    setSelectedRole({ value: det.roleDto.roleId, label: det.roleDto.roleName });
    setKey("Add");
    setOnEdit(true);
  };

  // todo==> UPDATE USER DATA
  const updatedata = async (e) => {
    e.preventDefault();
    // console.log(categoryId)
    var updatedata = {
      ...userdata,
      categoryId,
      updatedBy: { userId: user.userId },
      roleDto: { role: roleId },
    };
    console.log(updatedata);

    const respp = await updatedUser(headers, updatedata);
    console.log(respp);
    setKey("list-view");
    setSelectedRole({ value: "0", label: "Select..." });
    setRoleId("");
    FetchData();
  };

  
// todo ==> DELETE USER DATA
const handleRemove = async(userId) => {
  await deleteUser(userId,headers);
  FetchData();
 };

  return (
    <>
      <div className="pagetitle">
        <h1>UserList</h1>
        <nav>
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item active">User-List</li>
          </ol>
        </nav>

        <Card >
          <Card.Body>
            <Tabs
              activeKey={key}
              onSelect={(key) => {
                setKey(key);
              }}
              className="mb-3"
            >
              <Tab eventKey="Add" title={onedit ? "Edit" : "Add"}>
                <Form>
                  <Row className="mb-3">
                    <Form.Group as={Col} controlId="formGridfullName">
                      <Form.Label>Full Name</Form.Label>
                      <Form.Control
                        type="text"
                        name="fullName"
                        value={userdata.fullName}
                        placeholder="Enter fullName "
                        onChange={(e) => {
                          changehandler(e);
                          setField("fullName");
                        }}
                        isInvalid={!!errors.fullName}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.fullName}
                      </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridMobileNumber">
                      <Form.Label>Mobile Number</Form.Label>
                      <Form.Control
                        type="number"
                        name="mobileNumber"
                        value={userdata.mobileNumber}
                        placeholder="Enter Mobile number"
                        onChange={(e) => {
                          changehandler(e);
                          setField("mobileNumber");
                        }}
                        isInvalid={!!errors.mobileNumber}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.mobileNumber}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Row>
                  <Row className="mb-3">
                    <Form.Group as={Col} controlId="formGridEmail">
                      <Form.Label>Email</Form.Label>
                      <Form.Control
                        type="text"
                        name="email"
                        value={userdata.email}
                        placeholder="Enter Email"
                        onChange={(e) => {
                          changehandler(e);
                          setField("email");
                        }}
                        isInvalid={!!errors.email}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.email}
                      </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridState">
                      <Form.Label>Role:</Form.Label>
                      <Select
                        className={!!errors.selectedRole && "red-border"}
                        defaultValue={selectedRole}
                        value={selectedRole}
                        onChange={(value) => handleRoleChange(value)}
                        options={roles}
                      />
                      {roleValid && (
                        <h6 style={{ color: "red" }}>{errors.selectedRole}</h6>
                      )}
                    </Form.Group>
                  </Row>

                  <Row className="mb-3">
                    <Form.Group as={Col} controlId="formGridUserName">
                      <Form.Label>User Name</Form.Label>
                      <Form.Control
                        type="text"
                        name="userName"
                        value={userdata.userName}
                        placeholder="Enter user name"
                        onChange={(e) => {
                          changehandler(e);
                          setField("userName");
                        }}
                        isInvalid={!!errors.userName}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.userName}
                      </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridpassword">
                      <Form.Label>Password</Form.Label>
                      <Form.Control
                        type="password"
                        name="password"
                        value={userdata.password}
                        placeholder="Enter password"
                        onChange={(e) => {
                          changehandler(e);
                          setField("password");
                        }}
                        isInvalid={!!errors.password}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.password}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Row>
                  {onedit ? (
                    <Button
                      variant="primary"
                      type="submit"
                      onClick={(e) => {
                        updatedata(e);
                      }}
                    >
                      Update
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      type="submit"
                      onClick={(e) => {
                        postdata(e);
                      }}
                    >
                      Submit
                    </Button>
                  )}
                </Form>
              </Tab>
              <Tab eventKey="list-view" title="List">
              <div className="table-responsive-container">
              <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Full Name</th>
                      <th>User Name</th>
                      <th>Mobile NUmber</th>
                      <th>Email</th>
                      <th>Password</th>
                      <th>Role</th>
                      <th>Updated Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {usser.map((s) => {
                      return (
                        <tr>
                          <td>{s.userId}</td>
                          <td>{s.fullName}</td>
                          <td>{s.userName}</td>
                          <td>{s.mobileNumber}</td>
                          <td>{s.email}</td>
                          <td>{s.password}</td>
                          <td>{s.roleDto}</td>
                          <td>{s.updatedDate}</td>
                          <td>
                            <Button
                              variant="success"
                              className="m-2"
                              onClick={() => {
                                handleEdit(s.userId);
                              }}
                            >
                              Edit
                            </Button>
                            <Button variant="danger" onClick={()=>{handleRemove(s.userId)}}>Delete</Button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              </div>
                
              </Tab>
            </Tabs>
          </Card.Body>
        </Card>
      </div>
    </>
  );
};

export default Users;
