import React, { useState } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import Navbar from "react-bootstrap/Navbar";
// import logo from "https://saibiz.in/img/logo.png";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import baseUrl from "./BaseUrl"; // Adjust the import path based on your project structure
import './login.css'

const LoginSplitPage = () => {
  const [loginInfo, setLoginInfo] = useState({
    username: "",
    password: "",
  });

  const logo = "https://saibiz.in/img/logo.png";

  const [errors, setErrors] = useState({});

  const navi = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: "",
    }));

    setLoginInfo((prevLoginInfo) => ({
      ...prevLoginInfo,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const formErrors = validateForm();
    if (Object.keys(formErrors).length > 0) {
      setErrors(formErrors);
    } else {
      const userData = {
        username: loginInfo.username,
        password: loginInfo.password,
      };

      axios
        .post(`${baseUrl}/auth/login`, userData)
        .then((res) => {
          if (res.data.token) {
            sessionStorage.setItem("user", JSON.stringify(res.data));
            navi("/dash");
            alert("Welcome user");
          } else {
            alert("Bad credentials");
          }
        })
        .catch((error) => {
          console.error("Error logging in:", error);
          alert("An error occurred while logging in");
        });
    }
  };

  const validateForm = () => {
    const formErrors = {};
    if (!loginInfo.username) {
      formErrors.username = "Username is required";
    }
    if (!loginInfo.password) {
      formErrors.password = "Password is required";
    }
    return formErrors;
  };

  return (
    <div className="maincontainer">
      <Navbar bg="info" variant="dark">
        <Container>
          <Navbar.Brand href="#home">
            <img
              alt=""
              src={logo}
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{" "}
            <span className="text-white">Saibiz Innovations</span>
          </Navbar.Brand>
        </Container>
      </Navbar>
      <Container fluid>
        <Row className="no-gutter">
          <Col md={6} className="d-none d-md-flex bg-image">
            <img
              src="https://images.pexels.com/photos/1350789/pexels-photo-1350789.jpeg"
              alt="Background"
              className="img-fluid"
            />
          </Col>
          <Col md={6} className="bg-white d-flex align-items-center justify-content-center">
            <div className="login">
              <Container>
                <Row>
                  <Col lg={10} xl={7} className="mx-auto">
                    <h3 className="display-4 text-center mb-4">Login page!</h3>

                    <Form onSubmit={handleSubmit}>
                      <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>User Name</Form.Label>
                        <Form.Control
                          type="text"
                          name="username"
                          placeholder="Enter username"
                          value={loginInfo.username}
                          onChange={handleChange}
                          isInvalid={!!errors.username}
                        />
                        <Form.Control.Feedback type="invalid">
                          {errors.username}
                        </Form.Control.Feedback>
                      </Form.Group>

                      <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                          type="password"
                          name="password"
                          placeholder="Password"
                          value={loginInfo.password}
                          onChange={handleChange}
                          isInvalid={!!errors.password}
                        />
                        <Form.Control.Feedback type="invalid">
                          {errors.password}
                        </Form.Control.Feedback>
                      </Form.Group>

                      <Button
                        type="submit"
                        className="btn btn-primary btn-block text-uppercase mb-2 rounded-pill shadow-sm"
                      >
                        Sign in
                      </Button>
                    </Form>
                  </Col>
                </Row>
              </Container>
            </div>
          </Col>
        </Row>
      </Container>
      <footer className="footer bg-light">
        <Container fluid>
          <Row className="py-4">
            <Col xs={12} md={6} className="d-flex align-items-center">
              <small className="ms-2 text-black">
                &copy; Saibiz Innovations, 2024. All rights reserved.
              </small>
            </Col>
          </Row>
        </Container>
      </footer>
    </div>
  );
};

export default LoginSplitPage;
