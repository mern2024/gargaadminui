import axios from "axios";


//todo ==> POST CANDIADTE  DATA
export const  postCandiData=async(headers,cadat)=>{
    await axios({
        method:"POST",
        url:"https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/candidate/v1/createCandidate",
        headers:headers,
        data:JSON.stringify(cadat)
    }).then(function(res){
        console.log(res)
    })
}

//todo ==> GET  TOPIC DATA
export const fetchCandiData=async(headers)=>{
     return await axios({
        method:"GET",
        url:"https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/candidate/v1/getAllCandidatesByPagination/{pageNumber}/{pageSize}?pageNumber=0&pageSize=10",
        headers:headers
    })
}