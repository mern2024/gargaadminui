import React from 'react'
// import {Carousel} from '3d-react-carousal';
import image1 from '../images/React.png';
import image2 from '../images/java.png';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';





const Firstdashboard = () => {

 
    
   
         
      
  return (
    <>
     <div className="pagetitle">
            <h1>Dashboard</h1>
            <nav>
              <ol className="breadcrumb">
                <li className="breadcrumb-item">Home</li>
                <li className="breadcrumb-item active">Dashboard</li>
              </ol>
            </nav>
          </div>
      <div>
     
      </div>
      <br/><br/>
      <section class="section dashboard">
          <div class="col-lg-12">
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="card info-card sales-card">
                  <div class="card-body">
                    <h5 class="card-title">Category</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-tv"></i>
                      </div>
                      <div class="ps-3">
                        {/* <h6>{countData.promoCount}</h6> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
             
              <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="card info-card customers-card">
                  <div class="card-body">
                    <h5 class="card-title">Product</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-card-list"></i>
                      </div>
                      <div class="ps-3">
                        {/* <h6>{countData.categoryCount}</h6> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="card info-card user-card">
                  <div class="card-body">
                    <h5 class="card-title">Fatured</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-people-fill"></i>
                      </div>
                      <div class="ps-3">
                        {/* <h6>{countData.userProfileCount}</h6> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="card info-card sales-card">
                  <div class="card-body">
                    <h5 class="card-title">Users</h5>
                    <div class="d-flex align-items-center">
                      <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-badge-ad-fill"></i>
                      </div>
                      <div class="ps-3">
                        {/* <h6>{countData.bannerCount}</h6> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </>
  )
}

export default Firstdashboard