import axios from "axios";

//todo ==> POST PROMO DATA
export const postPromoData =async(pdata, headers) => {
  try {
    await axios({
      method: "POST",
      url: "https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/promo/v1/createPromo",
      headers: headers,
      data: JSON.stringify(pdata),
    }).then(function (res) {
      console.log(res);
      if (res.data.responseCode === 201) {
        alert("Promo  added successfully");
      } else {
        alert(res);
      }
    });
  } catch (error) {
    alert(error);
  }
};


//todo ==> GET  PROMO DATA
export const fetchPromo=async(headers)=>{
    return await axios({
    method:"GET",
    url:"https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/promo/v1/getAllPromoByPagination/{pageNumber}/{pageSize}?pageNumber=0&pageSize=10",
    headers:headers,
  })
}

//todo ==> GET DATA BY PROMO ID

export const getPromoById=async(headers,id)=>{
   return await axios({
    method:"GET",
    url:`https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/promo/v1/getPromoByPromoId/{promoId}?promoId=${id}`,
    headers:headers,
  })
}


 // todo==> UPDATE PROMO
 export const updatedPromo=async(headers,updateddata)=>{
   await axios({
    method:"PUT",
    url:"https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/promo/v1/updatePromo",
    headers:headers,
    data:updateddata
  }).then(function (res) {
    console.log(res);
  if (res.data.responseCode === 201) {
    alert(res.data.message);
  } else if (res.data.responseCode === 400) {
    alert(res.data.errorMessage);
  }
})
.catch(function (error) {
  console.log(error);
});
 }

//todo ==> DELETE  PROMO DATA

export const deletePromo=async(headers,id)=>{
   await axios({
    method:"DELETE",
    url:`https://virtullearning.cloudjiffy.net/BitStreamIOLMSWeb/promo/v1/deletePromoById/${id}`,
    headers:headers
  }).then((res) => {
    if (res.data.responseCode === 200) {
      alert(res.data.message);
    } else if (res.data.responseCode === 400) {
      alert(res.data.errorMessage);
    }
  })
  .catch((err) => {
    console.log(err);
  });
}