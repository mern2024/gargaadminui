import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Login from './components/Login';
import Dashboard from './components/Dashboard';
import Promo from './components/Promo';
import Category from './components/Category';
import Routing from './components/Routing';
import Firstdashboard from './components/Firstdashboard';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login/>}/>
          <Route path="/dash" element={<Dashboard/>}>
          <Route index element={<Firstdashboard/>}/>
         {Routing.map((item,index)=>{
          return <Route key={index} exact path={item.path} element={item.element}/>
         })}

          </Route>
          
          

        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
